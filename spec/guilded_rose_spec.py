from expects import *
import mamba

from src.guilded_rose import GuildedRose

from src.items.default import Default
from src.items.aged_brie import AgedBrie
from src.items.sulfuras import Sulfuras
from src.items.backstage_passes import BacksagePasses

with description("Kata Guilded Rose") as self:

    with description("Default item") as self:

        with it("When a day passes the values degrades"):
            quality = 50 
            sell_date = 10
            item = Default(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 5
            resulted_quality = 45
            
            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality)) 

        with it("Once the sell by date has passed, quality degrades twice as fast"):
            quality = 50 
            sell_date = 10
            item = Default(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 20
            resulted_quality = 20

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))         

        with it("The quality of an item is never negative"):
            quality = 50 
            sell_date = 10
            item = Default(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 40
            resulted_quality = 0

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))    

        with it("The quality of an item is never more than 50"):
            quality = 60 
            sell_date = 10
            item = Default(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 0
            resulted_quality = 50

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

    with description("Aged Brie") as self:

        with it('"Aged Brie" actually increases in quality the older it gets'):
            quality = 30
            sell_date = 10
            item = AgedBrie(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 5
            resulted_quality = 35

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

    with description("Sulfuras") as self:

        with it('"Sulfuras" never has to be sold or decreases in quality'):
            quality = 80
            sell_date = 0
            item = Sulfuras(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 5
            resulted_quality = 80

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

        with it('"Sulfuras" is a legendary item and as such its quality is 80 and it never alters'):

            quality = 10
            sell_date = 15
            item = Sulfuras(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 10
            resulted_quality = 80

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality)) 

    with description("Backstage passes") as self:

        with it('"Backstage passes" quality increases by 1 when there are more than 10 days till the sell date'):
            quality = 10
            sell_date = 20
            item = BacksagePasses(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 5
            resulted_quality = 15

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

        with it('"Backstage passes" quality increases by 2 when there are 10 days or less till the sell date'):
            quality = 10
            sell_date = 30
            item = BacksagePasses(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 24
            resulted_quality = 38

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

        with it('"Backstage passes" quality increases by 3 when there are 5 days or less till the sell date'):
            quality = 10
            sell_date = 30
            item = BacksagePasses(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 27
            resulted_quality = 46

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

        with it('"Backstage passes" quality drops to 0 after the concert'):
            quality = 10
            sell_date = 30
            item = BacksagePasses(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 30
            resulted_quality = 0

            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality))  

    with description("Conjured Items") as self:

        with it('"Conjured" items degrade in quality twice as fast as normal items'):

            quality = 10
            sell_date = 15
            item = BacksagePasses(quality, sell_date)
            guildedRose = GuildedRose(item)
            
            days = 10
            resulted_quality = 40

            item.conjured()
            guildedRose.daysGone(days)
            result = guildedRose.getValue()

            expect(result).to(equal(resulted_quality)) 
