class GuildedRose():

    def __init__(self, item):
        self.item = item

    def daysGone(self, days):
        self.item.degradation_rules(days)
    
    def getValue(self):
        return self.item.getQuality()


