from src.items.default import Default

DOUBLE_INCREMENT = 2
TRIPLE_INCREMENT = 3

DATE_INCREMENT_BY_TWO = 10
DATE_INCREMENT_BY_THREE = 5

NO_QUALITY = 0

class BacksagePasses(Default):
  
  def degradation_rules(self, days):
    self.__days_increment_by_one(days)

    self.__days_increment_by_two(days)
    
    self.__days_increment_by_three(days)

    self.__days_exceeds_limits(days)

  def __days_increment_by_one(self, days):
    past_days = min(days, self.sell_date - DATE_INCREMENT_BY_TWO)
    self.changed_quaility += past_days

  def __days_increment_by_two(self, days):
    if days > self.sell_date - DATE_INCREMENT_BY_TWO:
      past_days = min((days - (self.sell_date - DATE_INCREMENT_BY_TWO)), DATE_INCREMENT_BY_THREE)
      self.changed_quaility += past_days * DOUBLE_INCREMENT

  def __days_increment_by_three(self, days):
    if days >= self.sell_date - DATE_INCREMENT_BY_THREE:
      past_days = min((days - (self.sell_date - DATE_INCREMENT_BY_THREE)), DATE_INCREMENT_BY_THREE)
      self.changed_quaility += past_days * TRIPLE_INCREMENT

  def __days_exceeds_limits(self, days):
    if days >= self.sell_date:
      self.changed_quaility = NO_QUALITY
      self.quality = NO_QUALITY
