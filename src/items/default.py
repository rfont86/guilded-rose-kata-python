DOUBLE_DEGRADATION = 2
MAXIMUN_QUALITY = 50
MINIMUN_QUALITY = 0
LEGENDARY_QUALITY = 80
INITAL_QUALITY = 0

class Default():

  def __init__(self, quality, sell_date):
    self.quality = quality
    self.sell_date = sell_date

    self.changed_quaility = INITAL_QUALITY
    self.total_quality = INITAL_QUALITY
    
    self.is_conjured = False
    self.is_legendary = False

  def degradation_rules(self, days):
    if days < self.sell_date:
      self.changed_quaility -= days
    else:
      self.changed_quaility -= self.sell_date
      self.changed_quaility -= (days - self.sell_date) * DOUBLE_DEGRADATION

  def getQuality(self):
    if self.is_conjured:
      self.changed_quaility *= DOUBLE_DEGRADATION
    
    self.__quality_limits()
    
    return self.total_quality

  def conjured(self):
    self.is_conjured = True

  def __quality_limits(self):
    if self.is_legendary:
      self.total_quality = LEGENDARY_QUALITY
    else:
      self.total_quality = self.quality + self.changed_quaility
      if self.total_quality > MAXIMUN_QUALITY:
        self.total_quality = MAXIMUN_QUALITY
      if self.total_quality < MINIMUN_QUALITY:
        self.total_quality = MINIMUN_QUALITY